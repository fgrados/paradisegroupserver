<%--
  Created by IntelliJ IDEA.
  User: Fiorella
  Date: 02/03/2016
  Time: 10:30
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html"
        pageEncoding="ISO-8859-1" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>employee Registration</title>

    <style>
        tr:first-child{
            font-weight: bold;
            background-color: #858585;
        }
    </style>
</head>
<body>
    <h2>List of Employees</h2>
    <table>
        <tr>
            <td>Name</td><td>Lastname</td><td>SLastname</td><td>DNI</td><td>Dob</td><td>Joining date</td>
        </tr>
        <%
        %>
        <c:forEach items="${employees}" var="employee">
            <tr>
                <td>${employee.fName}</td>
                <td>${employee.lName}</td>
                <td>${employee.mLname}</td>
                <td>${employee.dni}</td>
                <td>${employee.dob}</td>
                <td>${employee.workSince}</td>
            </tr>
        </c:forEach>
    </table>

<br/>

</body>
</html>
