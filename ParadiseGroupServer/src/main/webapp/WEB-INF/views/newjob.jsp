<%--
  Created by IntelliJ IDEA.
  User: Fiorella
  Date: 03/03/2016
  Time: 10:15
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employee Registration</title>
</head>
<style>
    .error{
        color: red;
    }
</style>
<body>
<h2>Employee Registration</h2>
<form:form method="POST" modelAttribute="job">
    <table>
        <tr>
            <td><label for="jobName">Puesto: </label></td>
            <td><form:input id="jobName" path="jobName"/></td>
            <td><form:errors cssClass="error"/></td>
        </tr>
        <tr>
            <td><label for="salary">Sueldo: </label></td>
            <td><form:input id="salary" path="salary"/></td>
            <td><form:errors cssClass="error"/></td>
        </tr>
        <tr>
            <td colspan="3">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Register"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>
