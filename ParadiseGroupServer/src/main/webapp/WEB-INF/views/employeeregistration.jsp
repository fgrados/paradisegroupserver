<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Fiorella
  Date: 03/03/2016
  Time: 14:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employee Registration</title>
</head>
<style>
    .error{
        color: red;
    }
</style>
<body>
    <h2>Employee Registration</h2>
    <form:form method="POST" modelAttribute="employee">
        <table>
            <tr>
                <td><label for="fName">Nombre: </label></td>
                <td><form:input id="fName" path="fName"/></td>
                <td><form:errors cssClass="error"/></td>
            </tr>
            <tr>
                <td><label for="lName">Apellido paterno: </label></td>
                <td><form:input id="lName" path="lName"/></td>
                <td><form:errors cssClass="error"/></td>
            </tr>
            <tr>
                <td><label for="mLname">Apellido materno: </label></td>
                <td><form:input id="mLname" path="mLname"/></td>
                <td><form:errors cssClass="error"/></td>
            </tr>
            <tr>
                <td><label for="dob">Fecha de nacimiento: </label></td>
                <td><form:input id="dob" path="dob"/></td>
                <td><form:errors cssClass="error"/></td>
            </tr>
            <tr>
                <td><label for="workSince">Fecha de inicio: </label></td>
                <td><form:input id="workSince" path="workSince"/></td>
                <td><form:errors cssClass="error"/></td>
            </tr>
            <tr>
                <td><label for="dni">DNI: </label></td>
                <td><form:input id="dni" path="dni"/></td>
                <td><form:errors cssClass="error"/></td>
            </tr>
            <tr>
                <td><label for="address">Domicilio: </label></td>
                <td><form:input id="address" path="address"/></td>
                <td><form:errors cssClass="error"/></td>
            </tr>
            <tr>
                <td><label for="job_id">Jobb: </label></td>
                <td><form:input id="job_id" path="job.id"/></td>
                <td><form:errors cssClass="error"/></td>
            </tr>
            <tr>
                <td colspan="3">
                    <c:choose>
                        <c:when test="${edit}">
                            <input type="submit" value="Update"/>
                        </c:when>
                        <c:otherwise>
                            <input type="submit" value="Register"/>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </table>
    </form:form>
</body>
</html>
