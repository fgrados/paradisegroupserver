<%--
  Created by IntelliJ IDEA.
  User: Fiorella
  Date: 03/03/2016
  Time: 10:15
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html"
        pageEncoding="ISO-8859-1" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Register Hotel</title>
</head>
<style>
    .error{
        color: red;
    }
</style>
<body>

    <h2>Hotel Registration Form</h2>
    <form:form method="POST" modelAttribute="employee">
        <form:input path="id" id="id" type="hidden"/>
        <table>
            <tr>
                <td><label for="hotel_name">Name: </label> </td>
                <td><form:input path="hotel_name" id="hotel_name"/></td>
                <%--<td><form:errors path="hotel_name" /></td>--%>
            </tr>
            <tr>
                <td><label for="address">Name: </label> </td>
                <td><form:input path="address" id="address"/></td>
            </tr>

            <tr>
                <td colspan="3">
                    <c:choose>
                        <c:when test="${edit}">
                                <input type="submit" value="Update">
                        </c:when>
                        <c:otherwise>
                            <input type="submit" value="Register">
                        </c:otherwise>

                    </c:choose>
                </td>
            </tr>
        </table>
    </form:form>
<br/>
<br/>
Go back to <a href=""<c:url value='/' />">List of employees</a>
</body>
</html>
