<%--
  Created by IntelliJ IDEA.
  User: Fiorella
  Date: 04/03/2016
  Time: 09:02
  To change this template use File | Settings | File Templates.
--%>

<%@page language="java" contentType="text/html"
        pageEncoding="ISO-8859-1" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>employee Registration</title>

    <style>
        tr:first-child{
            font-weight: bold;
            background-color: #81a78b;
        }
    </style>
</head>
<body>
<h2>Puestos de trabajo</h2>
<table>
    <tr>
        <td>ID</td><td>Puesto</td><td>Sueldo</td>
    </tr>
    <%
    %>
    <c:forEach items="${jobs}" var="job">
        <tr>
            <td>${job.id}</td>
            <td>${job.jobName}</td>
            <td>${job.salary}</td>

        </tr>
    </c:forEach>
</table>

<br/>

</body>
</html>
