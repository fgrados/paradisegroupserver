package com.paradisegroup.server.service;

import com.paradisegroup.server.dao.RoomDao;
import com.paradisegroup.server.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fiorella.
 */
@Service("roomService")
@Transactional
public class RoomServiceImpl implements RoomService {

    @Autowired
    RoomDao dao;

    public void updateRoom(Room room) {
        dao.updateRoom(room);
    }

    public List<Room> findAvailable(boolean isAvailable) {
        return dao.findAvailable(isAvailable);
    }

    public Room getById(int id) {
        return dao.getById(id);
    }

}
