package com.paradisegroup.server.service;

import com.paradisegroup.server.model.Room;

import java.util.List;

/**
 * Interface
 * Created by Fiorella.
 */
public interface RoomService {
    void updateRoom(Room room);

    Room getById(int id);

    List<Room> findAvailable(boolean isAvailable);
}
