package com.paradisegroup.server.service;

import com.paradisegroup.server.dao.RegisterDao;
import com.paradisegroup.server.model.Register;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Fiorella.
 */
@Service("registerService")
@Transactional
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    RegisterDao dao;

    public List<Register> getActiveRegister() {
        return dao.findActives();
    }

    public List<Register> getInactiveRegister() {
        return dao.findInactives();
    }

    public Register getLastByRoomId(int id) {
        return dao.findLastByRoomId(id);
    }

    public void checkin(Register register) {
        dao.saveRegister(register);
    }

    public void updateRegister(Register register) {
        dao.updateRegister(register);
    }

    public void deleteRegister(int id) {
        dao.deleteRegister(id);
    }

    public Register getById(int id) {
        return dao.findById(id);
    }
}
