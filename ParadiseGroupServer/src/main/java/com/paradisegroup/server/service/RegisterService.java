package com.paradisegroup.server.service;

import com.paradisegroup.server.model.Register;

import java.util.List;

/**
 * Interface
 * Created by Fiorella.
 */
public interface RegisterService {

    List<Register> getActiveRegister();

    List<Register> getInactiveRegister();

    Register getLastByRoomId(int id);

    void checkin(Register register);

    void updateRegister(Register register);

    void deleteRegister(int id);

    Register getById(int id);
}
