package com.paradisegroup.server.controller;

import com.google.gson.Gson;
import com.paradisegroup.server.model.Room;
import com.paradisegroup.server.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller that receives all Room requests.
 * Created by Fiorella.
 */

@Controller
@RequestMapping("rooms")
public class RoomController {

    @Autowired
    RoomService service;

    @Autowired
    MessageSource messageSource;

    /**
     * List all available rooms
     */
    @RequestMapping(value = {"/available"}, method = RequestMethod.GET)
    @ResponseBody
    public String getAvailables(@RequestParam(value = "available") boolean isAvailable) {

        List<Room> rooms = service.findAvailable(isAvailable);
        Gson gson = new Gson();
        return gson.toJson(rooms);
    }

    /**
     * Change room availability.
     */
    @RequestMapping(value = "/clean/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable("id") int id) {

        Room room = service.getById(id);

        room.setAvailable(!room.isAvailable());
        service.updateRoom(room);
    }

}
