package com.paradisegroup.server.controller;

import com.paradisegroup.server.service.RegisterService;
import com.paradisegroup.server.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Fiorella.
 */

@Controller
@RequestMapping("/")
public class AppController {

    @Autowired
    RoomService service;

    @Autowired
    RegisterService registerService;

    @Autowired
    MessageSource messageSource;

    /*
     * List all existing employees
     */
    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String showStart() {

        return "";
    }


}
