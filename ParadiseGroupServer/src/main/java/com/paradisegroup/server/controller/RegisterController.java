package com.paradisegroup.server.controller;

import com.google.gson.*;
import com.paradisegroup.server.model.Register;
import com.paradisegroup.server.service.RegisterService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Register Controller
 * REceive all Register requests.
 * Created by Fiorella.
 */

@Controller
@RequestMapping("register")
public class RegisterController {

    @Autowired
    RegisterService service;

    @Autowired
    MessageSource messageSource;

    private final GsonBuilder builder = new GsonBuilder()
            .registerTypeAdapter(DateTime.class, new DateTimeSerializer());
    private final Gson gson = builder.create();


    /**
     * List all register that did not check out
     */
    @RequestMapping(value = {"/actives"}, method = RequestMethod.GET)
    @ResponseBody
    public String getRegister() {
        List<Register> register = service.getActiveRegister();
        Gson gson = new Gson();
        return gson.toJson(register);
    }

    /**
     * List all register that did check out
     */
    @RequestMapping(value = {"/inactives"}, method = RequestMethod.GET)
    @ResponseBody
    public String getInactiveRegister() {
        List<Register> register = service.getInactiveRegister();
        Gson gson = new Gson();
        return gson.toJson(register);
    }

    /**
     * List all register that did check out
     */
    @RequestMapping(value = {"/lastreg/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public String getLastRegByRoomId(@PathVariable("id") int id) {
        Register register = service.getLastByRoomId(id);
        Gson gson = new Gson();
        return gson.toJson(register);
    }

    /**
     * Maps request to updates register
     */
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST)
    public void updateRegister(@Valid @RequestParam(value = "register", required = false) String register) {

        Register reg = gson.fromJson(register, Register.class);
        service.updateRegister(reg);
    }

    /**
     * Checkout register.
     */
    @RequestMapping(value = "/updatereg/{id}", method = RequestMethod.PUT)
    public void checkout(@PathVariable("id") String id) {

        Register reg = service.getById(Integer.parseInt(id));

        reg.setCheck_out(new DateTime());

        service.updateRegister(reg);
    }

    /**
     * Checkout register.
     */
    @RequestMapping(value = "/close/{id}", method = RequestMethod.PUT)
    public void closeReg(@PathVariable("id") int id) {

        Register reg = service.getById(id);

        reg.setActive(!reg.isActive());

        service.updateRegister(reg);

    }

    /**
     * Delete a register.
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") String id) {

        service.deleteRegister(Integer.parseInt(id));
    }

    /**
     * create new register
     */
    @RequestMapping(value = {"/new"}, method = RequestMethod.POST)
    @ResponseBody
    public String addRegister(@Valid @RequestParam(value = "register", required = true) String json) {

        Register register = gson.fromJson(json, Register.class);
        service.checkin(register);

        return "success";
    }

    /**
     * Date deserializer
     */
    public class DateTimeSerializer implements JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

        private static final String PATTERN = "dd/MM/yyyy HH:mm";
        final DateTimeFormatter fmt = DateTimeFormat.forPattern(PATTERN);

        public JsonElement serialize(DateTime src, Type typeOfSrc, JsonSerializationContext context) {
            String retVal = fmt.print(src);
            return new JsonPrimitive(retVal);
        }

        public DateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();
            Long millis = (jsonObject.get("iMillis")).getAsLong();

            try {
                DateTime date = new DateTime(millis);
                return date;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
