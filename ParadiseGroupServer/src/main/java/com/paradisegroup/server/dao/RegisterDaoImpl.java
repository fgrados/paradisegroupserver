package com.paradisegroup.server.dao;

import com.paradisegroup.server.model.Register;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Implementations of REgisterDao
 * Created by Fiorella.
 */

@Repository("registerDao")
public class RegisterDaoImpl extends AbstractDao<Integer, Register> implements RegisterDao {


    /**
     * Finds a REgister by ID
     *
     * @param id
     * @return Register
     */
    public Register findById(int id) {
        return getByKey(id);
    }

    /**
     * Sparar a new Register in DB
     *
     * @param register
     */
    public void saveRegister(Register register) {
        persist(register);
    }

    /**
     * Delete a register by ID.
     *
     * @param id
     */
    public void deleteRegister(int id) {
        Query query = getSession().createSQLQuery("DELETE FROM Register WHERE register_id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    /**
     * Updates a register
     *
     * @param register
     */
    public void updateRegister(Register register) {
        update(register);
    }

    /**
     * Find all active registers for rooms that are not available.
     *
     * @return List of registers
     */
    public List<Register> findActives() {

        Criteria criteria = createEntityCriteria();

        criteria.add(Restrictions.sqlRestriction("available=FALSE"));
        criteria.add(Restrictions.or(Restrictions.isNull("check_out"), Restrictions.gt("check_out", DateTime.now())));
        return (List<Register>) criteria.list();
    }

    /**
     * Find all active registers for rooms that are not available and has already checked out.
     *
     * @return List of registers
     */
    public List<Register> findInactives() {
        Criteria criteria = createEntityCriteria();

        criteria.add(Restrictions.sqlRestriction("available=FALSE"));
        criteria.add(Restrictions.or(Restrictions.isNotNull("check_out"), Restrictions.lt("check_out", DateTime.now())));
        criteria.add(Restrictions.sqlRestriction("active=TRUE"));
        return (List<Register>) criteria.list();
    }

    /**
     * Find the last register of a room with the given ID.
     *
     * @param id
     * @return Register
     */
    public Register findLastByRoomId(int id) {

        Criteria criteria = createEntityCriteria();

        criteria.add(Restrictions.sqlRestriction("id=" + id)); //eq("room.room_id",id));//
        criteria.addOrder(Order.asc("check_out"));
        List<Register> list = (List<Register>) criteria.list();
        if (!list.isEmpty()) return list.get(list.size() - 1);
        else return null;
    }
}
