package com.paradisegroup.server.dao;

import com.paradisegroup.server.model.Room;

import java.util.List;

/**
 * Interface
 * Created by Fiorella.
 */
public interface RoomDao {
    void saveRoom(Room room);

    void updateRoom(Room room);

    Room getById(int id);

    List<Room> findAvailable(boolean isAvailable);
//
//    List<Room> findAllRooms();
//    void deleteRoom(String id);
}
