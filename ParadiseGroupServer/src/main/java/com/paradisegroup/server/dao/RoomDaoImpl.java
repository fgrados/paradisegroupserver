package com.paradisegroup.server.dao;

import com.paradisegroup.server.model.Room;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Fiorella.
 */

@Repository("roomDao")
public class RoomDaoImpl extends AbstractDao<Integer, Room> implements RoomDao {

    /**
     * finds room by id
     *
     * @param id
     * @return Room
     */
    public Room getById(int id) {
        return getByKey(id);
    }

    /**
     * Save a Room in DB
     *
     * @param room
     */
    public void saveRoom(Room room) {
        persist(room);
    }

    /**
     * Update a room that already exists in DB
     *
     * @param room
     */
    public void updateRoom(Room room) {
        getSession().update(room);
    }

    /**
     * Find available rooms.
     *
     * @param isAvailable
     * @return List of Rooms
     */
    public List<Room> findAvailable(boolean isAvailable) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("available", isAvailable));
        return (List<Room>) criteria.list();
    }

//    public void deleteRoom(int id) {
//        Query query = getSession().createSQLQuery("DELETE FROM Room WHERE room_id = :id");
//        query.setInteger("id",id);
//        query.executeUpdate();
//    }
//
//    public List<Room> findAllRooms() {
//        Criteria criteria = createEntityCriteria();
//        return (List<Room>)criteria.list();
//    }
}
