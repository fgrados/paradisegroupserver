package com.paradisegroup.server.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

/**
 * Created by Fiorella.
 */
public abstract class AbstractDao<PK extends Serializable, T> {

    private final Class<T> persistentClass;

    /**
     * Returns the class that Hibernate class extends.
     */
    @SuppressWarnings("unchecked")
    public AbstractDao() {
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Returns session from session factory
     *
     * @return
     */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    public T getByKey(PK key) {
        return (T) getSession().get(persistentClass, key);
    }

    /**
     * store entity i DB
     */
    public void persist(T entity) {
        getSession().persist(entity);
    }

    /**
     * Delete entity from DB
     */
    public void delete(T entity) {
        getSession().delete(entity);
    }

    /**
     * Update entity.
     */
    public void update(T entity) {
        getSession().update(entity);
    }

    /**
     * Returns criteria for a specific class.
     */
    protected Criteria createEntityCriteria() {
        return getSession().createCriteria(persistentClass);
    }
}
