package com.paradisegroup.server.dao;


import com.paradisegroup.server.model.Register;

import java.util.List;

/**
 * Interface
 * Created by Fiorella.
 */
public interface RegisterDao {

    Register findById(int id);

    void saveRegister(Register register);

    Register findLastByRoomId(int id);

    void deleteRegister(int id);

    List<Register> findActives();

    List<Register> findInactives();

    void updateRegister(Register register);
}
