package com.paradisegroup.server.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Room model that represents table i DB.
 * Created by Fiorella.
 */



@Entity
@Table(name = "Room")
public class Room implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(name = "nr", nullable = false)
    private int number;

    @NotNull
    @Size(min = 50, max = 150)
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "available")
    private boolean available;

    @NotNull
    @Column(name = "price")
    private int price;

    /**
     * Setter & getter
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;

        Room room = (Room) o;

        if (id != room.id) return false;
        if (number != room.number) return false;
        if (available != room.available) return false;
        if (price != room.price) return false;
        return description.equals(room.description);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + number;
        result = 31 * result + description.hashCode();
        result = 31 * result + (available ? 1 : 0);
        result = 31 * result + price;
        return result;
    }
}
