package com.paradisegroup.server.model;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Model that represents a Register table in DB.
 * Created by Fiorella.
 */

@Entity
@Table(name = "Register")
public class Register implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "register_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;

    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "checkin", nullable = false)
    private DateTime check_in;

    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "checkout")
    private DateTime check_out;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "passenger_name")
    private String passengerName;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "passenger_lname")
    private String passengerLastname;

    @NotNull
    @Size(min = 8, max = 12)
    @Column(name = "dni")
    private String dni;

    @NotNull
    @Size(min = 10, max = 50)
    @Column(name = "address")
    private String address;

    @Column(name = "active")
    private boolean active;

    /**
    * Setter & getter
    */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public DateTime getCheck_in() {
        return check_in;
    }

    public void setCheck_in(DateTime check_in) {
        this.check_in = check_in;
    }

    public DateTime getCheck_out() {
        return check_out;
    }

    public void setCheck_out(DateTime check_out) {
        this.check_out = check_out;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerLastname() {
        return passengerLastname;
    }

    public void setPassengerLastname(String passengerLastname) {
        this.passengerLastname = passengerLastname;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Register)) return false;

        Register register = (Register) o;

        if (id != register.id) return false;
        if (!room.equals(register.room)) return false;
        if (!check_in.equals(register.check_in)) return false;
        if (check_out != null ? !check_out.equals(register.check_out) : register.check_out != null) return false;
        if (!passengerName.equals(register.passengerName)) return false;
        if (!passengerLastname.equals(register.passengerLastname)) return false;
        if (!dni.equals(register.dni)) return false;
        return address.equals(register.address);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + room.hashCode();
        result = 31 * result + check_in.hashCode();
        result = 31 * result + (check_out != null ? check_out.hashCode() : 0);
        result = 31 * result + passengerName.hashCode();
        result = 31 * result + passengerLastname.hashCode();
        result = 31 * result + dni.hashCode();
        result = 31 * result + address.hashCode();
        return result;
    }
}
